import argparse
import datetime
import os
import os.path as osp
import sys
import time
import matplotlib
import json
import torchvision.models as models
from resnet_model import resnet18, resnet152, resnet50, resnet101
from preact_resnet import preactresnet18, preactresnet152, preactresnet101
from utils import Logger, AverageMeter
from MLP import MLP
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
from torch.optim import lr_scheduler
import dataset_load

parser = argparse.ArgumentParser("Center Loss Example")
parser.add_argument('-d', '--dataset', type=str, default='MyDataset')
parser.add_argument('-j', '--workers', default=4, type=int,
                    help="number of data loading workers (default: 4)")
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--lr_cent', type=float, default=0.001, help="learning rate for center loss")
parser.add_argument('--max-epoch', type=int, default=500)
parser.add_argument('--stepsize', type=int, default=20)
parser.add_argument('--eval-freq', type=int, default=1)
parser.add_argument('--print-freq', type=int, default=50)
parser.add_argument('--gpu', type=str, default='0')
parser.add_argument('--seed', type=int, default=1)
parser.add_argument('--save-dir', type=str, default='log')
parser.add_argument('--path', type=str, default='dataset')
parser.add_argument('--when_save_model', type=int, default='100', help="save model at epoch x")
parser.add_argument('--plot', action='store_true', help="whether to plot features for every epoch")
parser.add_argument('--loss', type=str, default='Xent',help="loss function (CenterLoss, ContrastiveCenterLoss, ClassicContrLoss, Xent, ClassicCenterLoss")
parser.add_argument('--pretrained', type=str, default='False', help="load pretrained model")
parser.add_argument('--lambda0', type=float, default=1.)
parser.add_argument('--lambda1', type=float, default=1.)
args = parser.parse_args()

def main():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    sys.stdout = Logger(osp.join(args.save_dir, 'log_NOHDL_18' + args.dataset + ' gpu= ' + args.gpu + '.txt'))

    if use_gpu:
        print("Currently using GPU: {}".format(args.gpu))
        print("Learning rate optimizer {:}  Batch-size {:}".format(args.lr,args.batch_size,))
        print("Pretrained model: {:}".format(args.pretrained))
        cudnn.benchmark = True
        cudnn.enabled = True
        torch.cuda.manual_seed_all(args.seed)
    else:
        print("Currently using CPU")
        exit()

    path_annotation = '/home/risen/datasets-nas/cifar100/train_annotation.json'
    #path_annotation = '/home/supreme/datasets/cifar100/cifar100/train_annotation.json'
    j_file= open(path_annotation, "r")
    annotations = json.load(j_file)

    path_annotation1 = '/home/risen/datasets-nas/cifar100/test_annotation.json'
    #path_annotation1 = '/home/supreme/datasets/cifar100/cifar100/test_annotation.json'

    j_file_test = open(path_annotation1, "r")
    test_annotation = json.load(j_file_test)

    #LABELS_URL = 'https://s3.amazonaws.com/outcome-blog/imagenet/labels.json'
    #class_annotation_imagenet = {(value.split(','))[0].replace(' ','_'):int(key)  for (key, value) in requests.get(LABELS_URL).json().items()}

    if j_file: #and class_annotation_imagenet:
        print("Annotation loaded")
    else:
        print("Error annotation loading.. exit")
        exit()
    print("Dataset loading: {}".format(args.dataset))
    dataset = dataset_load.create(
        name=args.dataset, batch_size=args.batch_size, path=args.path,train_annotation=annotations, test_annotation=test_annotation
    )
    print("Dataset loaded")

    trainloader, testloader = dataset.trainloader, dataset.testloader


    if args.pretrained == 'True':
        model = MLP()
        pretrained_model=models.resnet18(pretrained=True)
        if model and pretrained_model:
            print("Model loaded")
            # dim_features = model.linear0.in_features
        else:
            print("Loading models error")

    else:
        print("No one model loaded")
        model = resnet18(num_classes=dataset.num_classes)
        #dim_features=model.linear1.in_features

    if use_gpu:
        model = nn.DataParallel(model).cuda()
        #pretrained_model = nn.DataParallel(pretrained_model).cuda()


    criterion_xent = nn.CrossEntropyLoss()
    criterion_xent_clusters0 = nn.CrossEntropyLoss()
    criterion_xent_clusters1 = nn.CrossEntropyLoss()
    criterion_xent_clusters2 = nn.CrossEntropyLoss()


    criterion_cent = None
    optimizer_centloss = None


    if args.loss == 'CenterLoss':
        print("CenterLoss set lambda= ",args.lambda0)
        print("Features dimension: ", dim_features)
        criterion_cent = CenterLoss(dim_hidden=dim_features, num_classes=dataset.num_classes, use_gpu=use_gpu,
                                    batch_size=args.batch_size)

    # optimizer_model = torch.optim.SGD(model.parameters(), lr=args.lr, weight_decay=5e-4, momentum=0.9)
    # optimizer_model = torch.optim.Adam(model.parameters(), lr=args.lr)

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, lr=args.lr)

    scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        if epoch == args.when_save_model:  # Save trained model
            torch.save(model, args.save_dir + "/model_saved/" +args.loss+'_'+ str(args.max_epoch)+'_'+ args.gpu + '.pt')
            print("Model trained saved using: ", args.loss)

        else:
            train(trainloader, model, criterion_xent, None, criterion_xent_clusters0,criterion_xent_clusters1,criterion_xent_clusters2,
                  optimizer, use_gpu, dataset.num_classes, epoch,testloader, None)

        print("==> Epoch {}/{}".format(epoch + 1, args.max_epoch))

        scheduler.step()

        if args.eval_freq > 0 and (epoch + 1) % args.eval_freq == 0 or (epoch + 1) == args.max_epoch:
            print("==> Test")
            #acc, err, acc0, err0,acc1, err1,acc2, err2  = test(model, testloader,None)
            acc, err = test(model, testloader, None)
            print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err))
            #print("Accuracy SubClusters0 (%): {}\t Error rate (%): {} Accuracy SubClusters1 (%): {}\t Error rate (%): {} Accuracy SubClusters2 (%): {}\t Error rate (%): {}".format(acc0, err0,acc1, err1,acc2, err2))


    elapsed = round(time.time() - start_time)
    elapsed = str(datetime.timedelta(seconds=elapsed))
    print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))
    #plot_accuracy_epochs(acc_list)
    #save_list(acc_list)



def train(trainloader, model, criterion_xent, criterion_cent, criterion_xent_clusters0,criterion_xent_clusters1,criterion_xent_clusters2,
          optimizer,
          use_gpu, num_classes, epoch, testloader, pretrained_model):
    model.train()
    # pretrained_model.eval()
    xent_losses = AverageMeter()
    xent_clusters0 = AverageMeter()
    xent_clusters1 = AverageMeter()
    xent_clusters2 = AverageMeter()

    #losses = AverageMeter()
    index_centroids1 = []
    all_features, all_labels, all_data, all_cl_str0, all_cl_str1 = [], [], [], [], []

    # for batch_idx, (data, labels, cl0, cl1, cl2) in enumerate(trainloader):
    #
    #     data, labels, cl0, cl1, cl2 = data.cuda(), labels.cuda(), cl0.cuda(), cl1.cuda(), cl2.cuda()
    #     outputs,pred_clusters0,pred_clusters1,pred_clusters2 = model(data)
    for batch_idx,(data, labels) in enumerate(trainloader):
        data, labels = data.cuda(), labels.cuda()
        outputs = model(data)

        loss_xent = criterion_xent(outputs, labels)
        # loss_xent_clusters0 = criterion_xent_clusters0(pred_clusters0, cl0)
        # loss_xent_clusters1 = criterion_xent_clusters1(pred_clusters1, cl1)
        # loss_xent_clusters2 = criterion_xent_clusters2(pred_clusters2, cl2)



        # if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss':
        #     # CenterLoss.centers=nn.Parameter(torch.tensor(batch_centers).float().cuda())
        #     loss_cent = criterion_cent(features, labels, index_centroids, batch_centers)
        #     # if epoch > 4: #Activate our multi-centerLoss
        #     loss = loss_cent*args.lambda0 + loss_xent + loss_xent_clusters0 + loss_xent_clusters1
        #     #loss = loss_cent * args.lambda0 + loss_xent
        #
        #     # else:
        #     # loss = loss_xent #freeze our multi-centerloss


        # if args.loss == 'ClassicCenterLoss':
        #     loss_cent = criterion_cent(features, labels)
        #     loss = loss_cent + loss_xent
        #     # optimizer_centloss.zero_grad()

        if args.loss == 'Xent':
            loss = loss_xent #+ loss_xent_clusters0 + loss_xent_clusters1 + loss_xent_clusters2

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        #losses.update(loss.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))
        # xent_clusters0.update(loss_xent_clusters0.item(), cl0.size(0))
        # xent_clusters1.update(loss_xent_clusters1.item(), cl1.size(0))
        # xent_clusters2.update(loss_xent_clusters2.item(), cl2.size(0))

        # if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss' or args.loss == 'ClassicCenterLoss':
        #     cent_losses.update(loss_cent.item(), labels.size(0))

        # if use_gpu:
        #     all_features.append(features.data.cpu().numpy())
        #     all_labels.append(labels.data.cpu().numpy())
        #     all_data.append(data.data.cpu().numpy())
        #     all_cl_str0.append(s_labels.data.cpu().numpy())
        #     #all_cl_str1.append(cl_str1.data.cpu().numpy())
        #
        #     #if args.loss == 'CenterLoss':
        #     #    index_centroids1.append(index_centroids.data.cpu().numpy())
        # else:
        #     all_features.append(features.data.numpy())
        #     all_labels.append(labels.data.numpy())
        #if (batch_idx + 1) % 15 == 0:
        #    test(model, testloader, True, num_classes)
        # if batch_idx == 2000:
        #     print("==> Test")
        #     acc, err, acc1, err1 = test(model, testloader, pretrained_model)
        #     print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err))
        #     print("Accuracy SubClusters0 (%): {}\t Error rate (%): {}".format(acc1, err1))
        #     exit()
        if (batch_idx + 1) % 100 == 0:

            #if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss' or args.loss == 'ClassicCenterLoss':
            if args.loss == 'Xent':
                print(
                    "Batch {}\t Main Xent Loss {:.6f} ({:.6f})".format(# XentClusters0 Loss {:.6f} ({:.6f}) XentClusters1 Loss {:.6f} ({:.6f}) XentClusters2 Loss {:.6f} ({:.6f})".format(
                        batch_idx + 1,
                        xent_losses.val,
                        xent_losses.avg,
                        # xent_clusters0.val,
                        # xent_clusters0.avg,
                        # xent_clusters1.val,
                        # xent_clusters1.avg,
                        # xent_clusters2.val,
                        # xent_clusters2.avg,
                    ))
            else:
                print("Batch {}\t CrossEntropy {:.6f} ({:.6f})".format(batch_idx + 1, xent_losses.val, xent_losses.avg))


def test(model, testloader,pretrained_model):
    model.eval()
    right = 0
    wrong = 0
    nothing = 0
    #confusion_matrix = np.zeros((107,107), dtype=int)
    correct, total, correct0, total0,correct1, total1, correct2, total2, = 0, 0, 0, 0, 0, 0,0,0
    with torch.no_grad():
        # for data, labels, cl0, cl1, cl2 in testloader:
        for data, labels in testloader:
            data, labels = data.cuda(), labels.cuda()
            outputs = model(data)
            # data, labels, cl0, cl1, cl2 = data.cuda(), labels.cuda(), cl0.cuda(), cl1.cuda(), cl2.cuda()
            # outputs, pred_clusters0, pred_clusters1, pred_clusters2 = model(data)

            _, predicted = torch.max(outputs.data, 1)
            # _, predicted_clusters0 = torch.max(pred_clusters0.data, 1)
            # _, predicted_clusters1 = torch.max(pred_clusters1.data, 1)
            # _, predicted_clusters2 = torch.max(pred_clusters2.data, 1)
            total += labels.size(0)
            # total0 += cl0.size(0)
            # total1 += cl1.size(0)
            # total2 += cl2.size(0)

            bool_predicted = (predicted == labels)
            # bool_predicted0 = (predicted_clusters0 == cl0)
            # bool_predicted1 = (predicted_clusters1 == cl1)
            # bool_predicted2 = (predicted_clusters2 == cl2)

            correct += bool_predicted.sum().float().item()
            # correct0 += bool_predicted0.sum().float().item()
            # correct1 += bool_predicted1.sum().float().item()
            # correct2 += bool_predicted2.sum().float().item()


    #         for i, j in zip(bool_predicted, bool_predicted1):
    #             i,j=i.item(), j.item()
    #             if i == False and j == True:
    #                 right += 1
    #             elif i == True and j == False:
    #                 wrong += 1
    #             else:
    #                 nothing += 1
    #
    #
    #         for t, p in zip(predicted_clusters0, s_labels):
    #             try:
    #                 confusion_matrix[int(t), int(p)] += 1
    #             except Exception:
    #                 print('Error',t,p)
    #                 exit()
    #
    # for z in range(confusion_matrix.shape[0]):
    #     print("Precision class: ",z, " ->",confusion_matrix[z][z]/confusion_matrix[z].sum())
    #
    # print("Report: ", right, wrong, nothing)

    acc = correct * 100. / total
    err = 100. - acc

    # acc0 = correct0 * 100. / total0
    # err0 = 100. - acc0
    # acc1 = correct1 * 100. / total1
    # err1 = 100. - acc1
    # acc2 = correct2 * 100. / total2
    # err2 = 100. - acc2
    return acc, err
    #return acc, err, acc0, err0,acc1, err1,acc2, err2


if __name__ == '__main__':
    main()
