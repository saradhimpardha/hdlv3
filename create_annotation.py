import json
import os

path_train = '/home/risen/datasets-nas/ImNet-2012/train/'
path_annotation_train = '/home/risen/datasets-nas/ImNet-2012/val'
folder_class = []
h_dict_idx = {}
idx = 0

tmp_depth = int(input('Insert depth level for hierachical categories'))

for i in (os.listdir(path_train)):
    if not i.endswith(".tar"):
        folder_class.append(i)
        h_dict_idx[i] = idx
        idx += 1

h_dict = {}


def insert_into_dict(i, n_files):
    for file_name in n_files:
        h_dict.setdefault(file_name, [])
        h_dict[file_name].append(i)


from nltk.corpus import wordnet as wn

all_categories = [[] for i in range(0, tmp_depth)]
all_categoriesidx = [[] for i in range(0, tmp_depth)]
counter_idx = [0 for i in range(0, tmp_depth)]
for idx, i in enumerate(folder_class):
    #str_clean = (i.split('_')[0])  # For cifar
    str_clean=(i.split('__')[0])  # For Imagenet
    try:
        ss = wn.synsets(str_clean)[0]
    except Exception:
        print(wn.synsets(str_clean))
        print("hol")
        exit()
    ss_root = ss.hypernym_paths()

    try:
        syn = [ss_root[0][s].lemma_names('eng')[0] for s in range(tmp_depth, 0, -1)]
        print(i)
        print(syn)
    except Exception:
        pass

    if idx > 100:
        exit()
    for idx_s, sy in enumerate(syn):
        if sy not in all_categories[idx_s]:
            all_categories[idx_s].append(sy)
            all_categoriesidx[idx_s].append(counter_idx[idx_s])
            n_files = os.listdir(path_train + '/' + i)
            insert_into_dict(counter_idx[idx_s], n_files)
            counter_idx[idx_s] += 1
        else:
            # get position
            get_idx = all_categories[idx_s].index(sy)
            n_files = os.listdir(path_train + '/' + i)
            insert_into_dict(get_idx, n_files)


with open(path_annotation_train+'train_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)


for j in range(0, tmp_depth):
    print('level ',j, 'Categories:',all_categories[j])
    for i, item in enumerate(all_categories[j]):
        print(i, item)


#Only for test set
path_test = '/home/risen/datasets-nas/cifar100/test/'
path_annotation_test = '/home/risen/datasets-nas/cifar100/'
folder_class = []
h_dict_idx = {}
idx = 0

for i in (os.listdir(path_test)):
    if not i.endswith(".tar"):
        folder_class.append(i)
        h_dict_idx[i] = idx
        idx += 1


for idx, i in enumerate(folder_class):
    str_clean = (i.split('_')[0])  # For cifar
    try:
        ss = wn.synsets(str_clean)[0]
    except Exception:
        print(wn.synsets(str_clean))
        print("exit")
        exit()
    ss_root = ss.hypernym_paths()
    syn = [ss_root[0][s].lemma_names('eng')[0] for s in range(tmp_depth, 0, -1)]
    for idx_s, sy in enumerate(syn):
            # get position
            get_idx = all_categories[idx_s].index(sy)
            n_files = os.listdir(path_test + '/' + i)
            insert_into_dict(get_idx, n_files)








with open(path_annotation_test+'test_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)

